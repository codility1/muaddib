

def get_optimal_path(pos, delta, A, lowest_costs):
    if pos > 0:
        for start in range(pos, -1, -1):
            if A[pos] - A[start - 1] >= delta:
                return lowest_costs[start - 1]
    return 0


def solution_barebones_lowest_path(A):
    lowest_costs = [2 for _ in range(len(A))]  # only the first value needs to be 2

    for i in range(1, len(A)):
        daily_path_cost = 2 + lowest_costs[i - 1]
        weekly_path_cost = 7 + get_optimal_path(i, 7, A, lowest_costs)
        monthly_path_cost = 25 + get_optimal_path(i, 30, A, lowest_costs)
        lowest_costs[i] = min(daily_path_cost, weekly_path_cost, monthly_path_cost)
    return lowest_costs[-1]
