# NOTE: All windows are defined as W = (first_element, last_element + 1)
#       so they work for slicing


MAX_COST = 100000 * 2  # the maximum number of elements as single days is the maximum cost


def d30_win(start_index, max_index):
    # the minimum window size that makes sense is 13 elements
    # anything less and it can't be lower cost than the
    # individual days
    return [start_index, min(max_index, start_index + 13)]


def d7_win(start_index, max_index):
    # the minimum window size that makes sense is 4 elements
    # anything less and it can't be lower cost
    return [start_index, min(max_index, start_index + 4)]


def calc_trips_in_range(window):
    num_days_in_range = (window[1] - window[0])
    return num_days_in_range


def calc_days_covered_by_window(A, window):
    return A[min(len(A), window[1]) - 1] - A[window[0]] + 1


def is_useful(window, useful_day_count):
    num_days_in_range = calc_trips_in_range(window)
    return num_days_in_range >= useful_day_count


def find_next_n_day_boundary(A, window, window_limit, N):
    start_date = A[window[0]]
    Nm1 = N - 1
    for i in range(window[1], window_limit + 1):
        if A[i - 1] - start_date > Nm1:
            final_idx = i - 1
            if A[final_idx - 1] - start_date <= Nm1:
                return final_idx
            else:
                return window[0] + 1  # this will be a useless N-day window of 1 item
    return window_limit


def create_window(A, i, max_days_in_window, window_fn):
    window = window_fn(i, len(A))
    if calc_days_covered_by_window(A, window) > max_days_in_window:
        window[1] = window[0] + 1
    else:
        window[1] = find_next_n_day_boundary(A, window, len(A), max_days_in_window)
    return window


def calc_window_path_cost(window, window_cost, window_min_length, lowest_costs_from_point):
    path_cost = MAX_COST
    if is_useful(window, window_min_length):
        path_cost = window_cost + lowest_costs_from_point[window[1]]
    return path_cost


def calc_window_cache(A, max_days_in_window, window_fn):
    window_cache = [None for _ in range(len(A))]
    window_0 = create_window(A, 0, max_days_in_window, window_fn)
    window_cache[0] = (window_0[0], window_0[1])
    for i in range(1, len(A)):
        prev_window = window_cache[i - 1]
        # each subsequent window will end at either the same day as the previous window
        # or at some point after this. Working this out in a forward direction saves
        # a lot of recalculation over any other approach.
        next_window_0 = prev_window[0] + 1  # advance the start of the window
        next_window_1 = find_next_n_day_boundary(A, (next_window_0, prev_window[1]), len(A), max_days_in_window)
        window_cache[i] = (next_window_0, next_window_1)
    return window_cache


def find_min_path_cost(A, lowest_costs):
    d30_window_cache = calc_window_cache(A, 30, d30_win)
    d7_window_cache = calc_window_cache(A, 7, d7_win)
    for i in range(len(A) - 1, -1, -1):
        # at each point we work out the cost for each option of starting with a individual trip, or
        # the best 7 or 30 day window we can find starting from this position. Because we are running
        # backwards, the lowest cost has already been calculated for any value to the right of the
        # current index
        d30_window_path_cost = calc_window_path_cost(d30_window_cache[i], 25, 13, lowest_costs)
        d7_window_path_cost = calc_window_path_cost(d7_window_cache[i], 7, 4, lowest_costs)
        d1_path_cost = 2 + lowest_costs[i + 1]
        lowest_cost_at_i = min(d1_path_cost, min(d7_window_path_cost, d30_window_path_cost))
        lowest_costs[i] = lowest_cost_at_i
    return lowest_costs[0]


def solution(A):
    # A = [a0, a1, a2, ..., an-1]
    # If we start at the end and iterate backwards, at each stage we can
    # assess the lowest cost option out of counting the current slow
    # as a single day, or as a 7 day or 30 day. The lowest cost option
    # is recorded in a cache which lists the lowest cost to the right
    # of each point. This approach need only visit each point once and
    # hence is linear.
    lowest_cost_from_point = [0 for _ in range(len(A) + 1)]  # cost past the end of the array is zero
    lowest_cost = find_min_path_cost(A, lowest_cost_from_point)
    return lowest_cost
