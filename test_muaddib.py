import unittest

import random

from muaddib import solution
from muaddib_no_window_cache import solution_no_window_cache
from muaddib_barebones import solution_barebones
from muaddib_barebones_lowest_path import solution_barebones_lowest_path
from muaddib_tiny import solution_tiny

SOLUTION_FUNCTIONS = [solution, solution_no_window_cache, solution_barebones, solution_barebones_lowest_path, solution_tiny]


class MuaddibTestCase(unittest.TestCase):

    def setUp(self):
        random.seed(129)  # so random

    def _test_core(self, expected, A, solution_functions):
        for solution_function in solution_functions:
            self.assertEqual(expected, solution_function(A))

    def test_given_example(self):
        self._test_core(11, [1, 2, 4, 5, 7, 29, 30], SOLUTION_FUNCTIONS)

    def test_a(self):
        self._test_core(7, [1, 3, 5, 6], SOLUTION_FUNCTIONS)

    def test_b(self):
        self._test_core(10, [5, 10, 15, 20, 25], SOLUTION_FUNCTIONS)

    def test_only_day_tickets(self):
        self._test_core(20, [1, 4, 7, 10, 13, 16, 19, 22, 25, 28], SOLUTION_FUNCTIONS)

    def test_only_7day_tickets_a(self):
        self._test_core(21, [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 20, 21], SOLUTION_FUNCTIONS)

    def test_only_7day_tickets_b(self):
        self._test_core(21, [1, 2, 3, 4, 5, 6, 30, 31, 32, 33, 34, 35, 36, 42, 43, 44, 45, 46, 47, 48], SOLUTION_FUNCTIONS)

    def test_60_consecutive_days(self):
        self._test_core(50, range(1, 61), SOLUTION_FUNCTIONS)

    def test_days_and_weeks_a(self):
        self._test_core(11, [11, 15, 16, 17, 18, 19, 20, 21, 25], SOLUTION_FUNCTIONS)

    def test_days_and_weeks_b(self):
        self._test_core(16, [48, 49, 50, 52, 53, 54, 56, 58, 59, 60, 62, 63, 64], SOLUTION_FUNCTIONS)

    def test_days_and_weeks_c(self):
        self._test_core(9, [5, 7, 8, 9, 10, 12, 13], SOLUTION_FUNCTIONS)

    def test_days_and_weeks_d(self):
        self._test_core(23, [4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 26, 27, 28], SOLUTION_FUNCTIONS)

    def test_c(self):
        self._test_core(13, [19, 20, 22, 23, 24, 25, 26, 27, 28, 30], SOLUTION_FUNCTIONS)

    def test_d(self):
        self._test_core(21, [8, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28], SOLUTION_FUNCTIONS)

    def test_e(self):
        self._test_core(36, [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 45, 46, 47, 48, 58, 59], SOLUTION_FUNCTIONS)


if __name__ == '__main__':
    unittest.main()
