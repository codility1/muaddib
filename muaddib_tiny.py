def solution_tiny(A):
    c = [2 for _ in range(len(A))]  # only the first value needs to be 2

    def find_cost(pos, N):
        for start in range(pos, -1, -1):
            if A[pos] - A[start - 1] >= N:
                return c[start - 1]
        return 0
    for i in range(1, len(A)):
        c[i] = min(2 + c[i - 1], 7 + find_cost(i, 7), 25 + find_cost(i, 30))
    return c[-1]
