
def get_best_last(pos, daily_paths, weekly_paths, monthly_paths):
    return min(daily_paths[pos - 1], weekly_paths[pos - 1], monthly_paths[pos - 1]) if pos > 0 else 0


def get_optimal_path(pos, delta, A, daily_paths, weekly_paths, monthly_paths):
    if pos == 0:
        return 0
    else:
        for start in range(pos, -1, -1):
            if A[pos] - A[start - 1] >= delta:
                return get_best_last(start, daily_paths, weekly_paths, monthly_paths)
        return 0


def solution_barebones(A):
    daily_paths = [0 for _ in range(len(A))]
    weekly_paths = [0 for _ in range(len(A))]
    monthly_paths = [0 for _ in range(len(A))]

    for i in range(len(A)):
        daily_paths[i] = 2 + get_best_last(i, daily_paths, weekly_paths, monthly_paths)
        weekly_paths[i] = 7 + get_optimal_path(i, 7, A, daily_paths, weekly_paths, monthly_paths)
        monthly_paths[i] = 25 + get_optimal_path(i, 30, A, daily_paths, weekly_paths, monthly_paths)

    return get_best_last(len(A), daily_paths, weekly_paths, monthly_paths)
