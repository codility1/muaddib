import random
import cProfile
import pstats

from muaddib import solution
from muaddib_recursive import solution_recursive
from muaddib_no_window_cache import solution_no_window_cache
from muaddib_barebones import solution_barebones
from muaddib_barebones_lowest_path import solution_barebones_lowest_path


def run_hard(test_data, function_to_profile):
    for idx, data in enumerate(test_data):
        print("Test run {}: len(A) = {}".format(idx, len(data)))
        for _ in range(10):
            function_to_profile(data)


def profile(test_data, functions_to_profile):
    for function_to_profile in functions_to_profile:
        print("Profiling function: {}".format(function_to_profile))
        pr = cProfile.Profile()
        pr.enable()
        run_hard(test_data, function_to_profile)
        pr.disable()
        pstats.Stats(pr).sort_stats('time', 'cumulative').print_stats()


def create_test_array(offsets):
    test_data = [0 for _ in range(len(offsets))]
    test_data[0] = offsets[0]
    for i in range(1, len(offsets)):
        test_data[i] = test_data[i-1] + offsets[i]
    return test_data


def test_correctness():
    test_data_offsets = [[random.randint(1, 8) for _ in range(x)] for x in [20, 50, 100, 1000, 5000, 10000, 50000, 80000]]
    test_data_array = [create_test_array(x) for x in test_data_offsets]
    for test_data in test_data_array:
        new_val = solution(test_data)
        old_val = solution_no_window_cache(test_data)
        print("Test data length = {}, old val={}, new val={}\n\n".format(len(test_data), old_val, new_val))
        if new_val != old_val:
            print("MISMATCH!")
        assert new_val == old_val


def main():
    random.seed(121)
    test_correctness()
    test_data = [[random.randint(1, 100000) for _ in range(50000)]]
    # profile(test_data, [solution, solution_no_window_cache, solution_recursive])
    profile(test_data, [solution, solution_no_window_cache, solution_barebones, solution_barebones_lowest_path])
    # profile(test_data, [solution])


if __name__ == '__main__':
    main()
