# NOTE: All windows are defined as W = (first_element, last_element + 1)
#       so they work for slicing


MAX_COST = 100000 * 2  # the maximum number of elements as single days is the maximum cost


def d30_win(start_index, max_index):
    # the minimum window size that makes sense is 13 elements
    # anything less and it can't be lower cost than the
    # individual days
    return [start_index, min(max_index, start_index + 13)]


def d7_win(start_index, max_index):
    # the minimum window size that makes sense is 4 elements
    # anything less and it can't be lower cost
    return [start_index, min(max_index, start_index + 4)]


def calc_trips_in_range(window):
    num_days_in_range = (window[1] - window[0])
    return num_days_in_range


def calc_days_covered_by_window(A, window):
    try:
        return A[min(len(A) - 1, window[1] - 1)] - A[window[0]] + 1
    except IndexError as ie:
        return 0


def is_useful_window(window, useful_day_count):
    num_days_in_range = calc_trips_in_range(window)
    return num_days_in_range >= useful_day_count


def find_next_n_day_boundary(A, window, window_limit, N):
    for i in range(window[1], window_limit + 1):
        if calc_days_covered_by_window(A, (window[0], i)) > N:
            final_idx = max(window[0] + 1, i - 1)
            if calc_days_covered_by_window(A, (window[0], final_idx)) <= N:
                return final_idx
            else:
                return window[0] + 1  # this will be a useless N-day window of 1 item
    return window_limit


def create_window(A, i, max_days_in_window, window_fn):
    window = window_fn(i, len(A))
    if calc_days_covered_by_window(A, window) > max_days_in_window:
        window[1] = window[0]
    else:
        window[1] = find_next_n_day_boundary(A, window, len(A), max_days_in_window)
    return window


def find_lowest_costs(pos, lowest_cost_from_point, path_cost_calculator):
    if pos in lowest_cost_from_point:
        return lowest_cost_from_point[pos]
    else:
        path_cost = path_cost_calculator(pos)
        lowest_cost_from_point[pos] = path_cost
        return path_cost


def calc_window_path_cost(A, start_pos, window_fn, window_cost, window_max_length, window_min_length, lowest_costs_from_point):
    window = create_window(A, start_pos, window_max_length, window_fn)
    path_cost = MAX_COST
    if is_useful_window(window, window_min_length):
        path_cost = window_cost + find_min_path_cost_recursive(A, window[1], lowest_costs_from_point)
    return path_cost


def find_min_path_cost_recursive(A, start_pos, lowest_costs_from_point):
    assert start_pos <= len(A)
    if start_pos == len(A):
        return 0
    else:
        d30_path_cost = find_lowest_costs(start_pos, lowest_costs_from_point,
                                          lambda x: calc_window_path_cost(A, x, d30_win, 25, 30, 13, lowest_costs_from_point))
        d7_path_cost = find_lowest_costs(start_pos, lowest_costs_from_point,
                                         lambda x: calc_window_path_cost(A, x, d7_win, 7, 7, 4, lowest_costs_from_point))
        d1_path_cost = find_lowest_costs(start_pos, lowest_costs_from_point,
                                         lambda x: 2 + find_min_path_cost_recursive(A, x + 1, lowest_costs_from_point))
        return min(min(d1_path_cost, d7_path_cost), d30_path_cost)


def solution_recursive(A):
    lowest_costs_from_point = {len(A): 0}
    lowest_cost = find_min_path_cost_recursive(A, 0, lowest_costs_from_point)
    return lowest_cost
