# NOTE: All windows are defined as W = (first_element, last_element + 1)
#       so they work for slicing


MAX_COST = 100000 * 2  # the maximum number of elements as single days is the maximum cost


def d30_win(start_index, max_index):
    # the minimum window size that makes sense is 13 elements
    # anything less and it can't be lower cost than the
    # individual days
    return [start_index, min(max_index, start_index + 13)]


def d7_win(start_index, max_index):
    # the minimum window size that makes sense is 4 elements
    # anything less and it can't be lower cost
    return [start_index, min(max_index, start_index + 4)]


def calc_trips_in_range(window):
    num_days_in_range = (window[1] - window[0])
    return num_days_in_range


def calc_days_covered_by_window(A, window):
    return A[min(len(A), window[1]) - 1] - A[window[0]] + 1


def is_useful_window(window, useful_day_count):
    num_days_in_range = calc_trips_in_range(window)
    return num_days_in_range >= useful_day_count


def find_next_n_day_boundary(A, window, window_limit, N):
    start_date = A[window[0]]
    Nm1 = N - 1
    for i in range(window[1], window_limit + 1):
        if A[i - 1] - start_date > Nm1:
            final_idx = i - 1
            if A[final_idx - 1] - start_date <= Nm1:
                return final_idx
            else:
                return window[0] + 1  # this will be a useless N-day window of 1 item
    return window_limit


def create_window(A, i, max_days_in_window, window_fn):
    window = window_fn(i, len(A))
    if calc_days_covered_by_window(A, window) > max_days_in_window:
        window[1] = window[0]
    else:
        window[1] = find_next_n_day_boundary(A, window, len(A), max_days_in_window)
    return window


def find_lowest_costs(pos, lowest_cost_from_point, path_cost_calculator):
    if pos in lowest_cost_from_point:
        return lowest_cost_from_point[pos]
    else:
        path_cost = path_cost_calculator(pos)
        lowest_cost_from_point[pos] = path_cost
        return path_cost


def calc_window_path_cost(A, start_pos, window_fn, window_cost, window_max_length, window_min_length, lowest_costs_from_point):
    window = create_window(A, start_pos, window_max_length, window_fn)
    path_cost = MAX_COST
    if is_useful_window(window, window_min_length):
        path_cost = window_cost + lowest_costs_from_point[window[1]]
    return path_cost


def find_min_path_cost(A, lowest_cost_from_point):
    for i in range(len(A) - 1, -1, -1):
        # at each point we work out the cost for each option of starting with a individual trip, or
        # the best 7 or 30 day window we can find starting from this position. Because we are running
        # backwards, the lowest cost has already been calculated for any value to the right of the
        # current index
        d30_window_path_cost = calc_window_path_cost(A, i, d30_win, 25, 30, 13, lowest_cost_from_point)
        d7_window_path_cost = calc_window_path_cost(A, i, d7_win, 7, 7, 4, lowest_cost_from_point)
        d1_path_cost = 2 + lowest_cost_from_point[i + 1]
        lowest_cost_at_i = min(d1_path_cost, min(d7_window_path_cost, d30_window_path_cost))
        lowest_cost_from_point[i] = lowest_cost_at_i
    return lowest_cost_from_point[0]


def solution_no_window_cache(A):
    # A = [a0, a1, a2, ..., an-1]
    # If we start at the end and iterate backwards, at each stage we can
    # assess the lowest cost option out of counting the current slow
    # as a single day, or as a 7 day or 30 day. The lowest cost option
    # is recorded in a cache which lists the lowest cost to the right
    # of each point. This approach need only visit each point once and
    # hence is linear.
    lowest_cost_from_point = [0 for _ in range(len(A)+1)]  # cost past the end of the array is zero
    lowest_cost = find_min_path_cost(A, lowest_cost_from_point)
    return lowest_cost
